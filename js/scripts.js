
window.addEventListener('load', (event) => {
    type();
});

//#region typingeffect
var speed = 100;
var speed2 = 100;
var str;
var i = 0;
var isRemoving = false;

var messages = [
    "Freelancer",
    "Web Developer",
    "C# Developer",
    "Junior Frontend"
]

function type() {
    str = document.getElementById('str');
    if (isRemoving) {
        if (str.innerText.length > 0) {
        str.innerText = str.innerText.substring(0, str.innerHTML.length - 1);
        setTimeout( type, speed2 );
        return;
        }
        isRemoving = false;
        i++;
        if (i >= messages.length) {
            i = 0;
        }
        setTimeout( type, speed );
        return;
    }
    var message = messages[i];
    str.innerText = message.substring(0, str.innerHTML.length + 1);
    if (str.innerText.length === message.length) {
        isRemoving = true;
    }
    setTimeout( type, isRemoving ? speed2 : speed );
}
//#endregion